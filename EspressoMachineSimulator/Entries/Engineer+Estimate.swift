// Created by Paulius Cesekas on 05/11/2018.

import Domain

extension Engineer {
    // MARK: - Variables
    var workingHoursEnd: Time {
        return workingHoursStart + Constants.workingHoursPerDay
    }
    
    // MARK: - Methods -
    @objc
    func isCoffeeTime(_ currentTime: Time) -> Bool {
        guard workingHoursStart < currentTime,
            workingHoursEnd > currentTime else {
                return false
        }
        
        return true
    }

}
