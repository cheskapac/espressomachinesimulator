// Created by Paulius Cesekas on 30/10/2018.

import Domain

class SimulatorEngineer: Engineer {
    // MARK: - Variables
    var superBusyStart: Time?
    var superBusyDuration: Time?
    var lastCoffeeTime: Time?

    // MARK: - Estimated Variables
    var superBusyEnd: Time? {
        guard let superBusyStart = superBusyStart,
            let superBusyDuration = superBusyDuration else {
                return nil
        }
        
        return superBusyStart + superBusyDuration
    }

    // MARK: - Methods -
    init(id: String, chanceToBecomeSuperBusy: Double) {
        super.init(id: id)
        let willBecomeSuperBusy = SimulatorEngineer
            .generateWillBecomeSuperBusy(withChance: chanceToBecomeSuperBusy)
        if willBecomeSuperBusy {
            generateSuperBusyStart()
            generateSuperBusyDuration()
        }
    }
    
    override func isCoffeeTime(_ currentTime: Time) -> Bool {
        guard super.isCoffeeTime(currentTime) else {
            return false
        }
        guard let lastCoffeeTime = lastCoffeeTime else {
            return true
        }
        
        let durationForCoffee = 1 * Time.hourInSeconds / coffeeConsumtionPerHour
        let timeToLastCoffee = currentTime - lastCoffeeTime
        return timeToLastCoffee >= durationForCoffee
    }
}

// MARK: - Generator
extension SimulatorEngineer {
    fileprivate static func generateWillBecomeSuperBusy(withChance chance: Double) -> Bool {
        let chanceToBecomeSuperBusy = clamp(
            chance,
            min: 0,
            max: 1)
        let random = Double.random(in: Range(uncheckedBounds: (0, 1)))
        return random <= chanceToBecomeSuperBusy
    }
    
    fileprivate func generateSuperBusyStart() {
        let range = Range(uncheckedBounds: (0, Constants.workingHoursPerDay))
        superBusyStart = workingHoursStart + Time.random(in: range)
    }
    
    fileprivate func generateSuperBusyDuration() {
        guard let superBusyStart = superBusyStart else {
            return
        }
        
        let maxSuperBusyDuration = workingHoursStart + Constants.workingHoursPerDay - superBusyStart
        let range = Range(uncheckedBounds: (0, maxSuperBusyDuration))
        superBusyDuration = Time.random(in: range)
    }
}

// MARK: - CustomStringConvertible
extension SimulatorEngineer: CustomStringConvertible {
    var description: String {
        return "\(id) - \(isSuperBusy ? "T" : "F")"
    }
}
