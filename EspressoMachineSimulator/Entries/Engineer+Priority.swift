// Created by Paulius Cesekas on 05/11/2018.

import Domain

extension Engineer {
    func isPrioritized(for priority: PriorityType) -> Bool {
        switch priority {
        case .superBusyFirst:
            return isSuperBusy
        }
    }
}
