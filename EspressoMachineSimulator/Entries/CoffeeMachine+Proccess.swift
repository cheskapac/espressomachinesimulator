// Created by Paulius Cesekas on 30/10/2018.

import Domain

extension CoffeeMachine {
    mutating func addToQueue(_ engineer: Engineer) {
        queue.append(engineer)
    }
    
    mutating func brew() -> Engineer? {
        guard let itemInProcess = itemInProcess else {
            if let item = nextInQueue() {
                updateProcessingItem(with: item)
                return brew()
            }
            return nil
        }
        
        itemInProcess.processTime += Time.second
        guard itemInProcess.processTime >= brewingDuration(for: itemInProcess.engineer.coffee) else {
            return nil
        }

        queue.removeAll { (engineer) -> Bool in
            engineer.id == itemInProcess.engineer.id
        }
        self.itemInProcess = nil
        return itemInProcess.engineer
    }
    
    private mutating func updateProcessingItem(with engineer: Engineer) {
        itemInProcess = ProcessingItem(engineer)
    }
    
    private func nextInQueue() -> Engineer? {
        guard !queue.isEmpty else {
            return nil
        }
        
        switch queueProcessing {
        case .firstComeFirstServe:
            for engineer in queue {
                if engineer.isPrioritized(for: queuePriority) {
                    return engineer
                }
            }
            return queue.first
        }
    }

    private func brewingDuration(for coffee: CoffeeType) -> TimeInterval {
        switch coffee {
        case .espresso:
            return .minuteInSeconds
        }
    }
}
