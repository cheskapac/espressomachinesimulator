// Created by Paulius Cesekas on 05/11/2018.

import Domain

// MARK: - CustomStringConvertible
extension Time {
    var formattedTwentyFourHours: String {
        return "\(Int(hours)):\(Int(minutesAfterHours)):\(Int(secondsAfterMinutes))"
    }
}
