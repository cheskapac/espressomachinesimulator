// Created by Paulius Cesekas on 30/10/2018.

import Foundation
import Domain

enum Constants {
    static let workingHoursPerDay: Time = 8 * Time.hourInSeconds
}
