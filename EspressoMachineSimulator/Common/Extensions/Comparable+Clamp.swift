// Created by Paulius Cesekas on 31/10/2018.

import Foundation

public func clamp<T>(_ value: T, min: T, max: T) -> T where T: Comparable {
    return Swift.min(Swift.max(value, min), max)
}
