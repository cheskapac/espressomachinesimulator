// Created by Paulius Cesekas on 30/10/2018.

import Foundation
import Domain

protocol SimulatorDelegate: class {
    func simulator(_ simulator: Simulator, didUpdate time: Time)
    func simulator(_ simulator: Simulator, didBrewItem item: Engineer)
    func simulatorDidComplete(_ simulator: Simulator)
}

class Simulator {
    // MARK: - Variables
    private var espressoMachine: CoffeeMachine
    private var companyEngineers: [SimulatorEngineer]
    private var speed: Double
    private var timer: Timer?
    private var currentTime: Time!
    weak var delegate: SimulatorDelegate?

    // MARK: - Methods -
    init(numberOfEngineers: Int, chanceToBecomeSuperBusy: Double, speed: Double) {
        espressoMachine = CoffeeMachine()
        companyEngineers = Simulator.generateEngineers(
            numberOfEngineers,
            chanceToBecomeSuperBusy: chanceToBecomeSuperBusy)
        self.speed = speed
    }
    
    func startSimulation() {
        currentTime = 0
        timer = Timer.scheduledTimer(
            timeInterval: 1 / speed,
            target: self,
            selector: #selector(tickSimulator),
            userInfo: nil,
            repeats: true)
        timer?.fire()
    }
    
    func reset() {
        timer?.invalidate()
        timer = nil
        currentTime = 0
        
        let companyEngineersCount = companyEngineers.count
        for i in 0..<companyEngineersCount {
            companyEngineers[i].lastCoffeeTime = nil
        }
    }
    
    // MARK: - Proccess
    @objc
    private func tickSimulator() {
        currentTime += Time.second
        delegate?.simulator(self, didUpdate: currentTime)
        updateSuperBusyStatus()
        requestCoffee()
        if let item = espressoMachine.brew() {
            delegate?.simulator(
                self,
                didBrewItem: item)
            print("Queue: \(espressoMachine.queue)")
        }
        if currentTime >= 24 * Time.hourInSeconds {
            delegate?.simulatorDidComplete(self)
            timer?.invalidate()
            timer = nil
            currentTime = 0
        }
    }
    
    private func updateSuperBusyStatus() {
        let companyEngineersCount = companyEngineers.count
        for i in 0..<companyEngineersCount {
            if companyEngineers[i].isSuperBusy,
                let superBusyEnd = companyEngineers[i].superBusyEnd,
                superBusyEnd < currentTime {
                    companyEngineers[i].isSuperBusy = false
            } else if !companyEngineers[i].isSuperBusy,
                let superBusyStart = companyEngineers[i].superBusyStart,
                superBusyStart <= currentTime,
                let superBusyEnd = companyEngineers[i].superBusyEnd,
                superBusyEnd >= currentTime {
                    companyEngineers[i].isSuperBusy = true
            }
        }
    }
    
    private func requestCoffee() {
        let companyEngineersCount = companyEngineers.count
        for i in 0..<companyEngineersCount {
            if companyEngineers[i].isCoffeeTime(currentTime) {
                espressoMachine.addToQueue(companyEngineers[i])
                companyEngineers[i].lastCoffeeTime = currentTime
            }
        }
    }
}

// MARK: - Generator
extension Simulator {
    fileprivate static func generateEngineers(_ engineersCount: Int, chanceToBecomeSuperBusy: Double) -> [SimulatorEngineer] {
        var engineers = [SimulatorEngineer]()
        for i in 0..<engineersCount {
            let id = "eng_0\(i)"
            let engineer = SimulatorEngineer(
                id: id,
                chanceToBecomeSuperBusy: chanceToBecomeSuperBusy)
            engineers.append(engineer)
        }
        return engineers
    }
    
    private static func generateBecomeSuperBusy(withChance chanceToBecomeSuperBusy: Double) -> Bool {
        let chanceToBecomeSuperBusy = clamp(
            chanceToBecomeSuperBusy,
            min: 0,
            max: 1)
        let random = Double.random(in: Range(uncheckedBounds: (0, 1)))
        return random <= chanceToBecomeSuperBusy
    }
    
    private static func generateSuperBusyStart() -> Time {
        return Time.random(in: Range(uncheckedBounds: (0, Constants.workingHoursPerDay)))
    }
    
    private static func generateSuperBusyDuration() -> Time {
        return Time.random(in: Range(uncheckedBounds: (0, Constants.workingHoursPerDay)))
    }
}
