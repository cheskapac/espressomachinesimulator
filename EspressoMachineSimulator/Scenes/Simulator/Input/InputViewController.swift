// Created by Paulius Cesekas on 05/11/2018.

import UIKit

class InputViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet private weak var countTextField: UITextField!
    @IBOutlet private weak var chanceTextField: UITextField!
    @IBOutlet private weak var submitButton: UIBarButtonItem!

    // MARK: - Methods -
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: - UI Actions
    @IBAction
    private func didTouchUpInsideSubmitButton() {
        guard let countText = countTextField.text,
            let count = Int(countText),
            count > 0 else {
                showError(message: "Please enter number of engineers [1..99+]")
                return
        }
        guard let chanceText = chanceTextField.text,
            let chance = Double(chanceText),
            chance >= 0,
            chance <= 1 else {
                showError(message: "Please enter number of chance that an engineer becomes super­busy in some unit of" +
                    "time, and for how long they stay super­busy [0..1]")
                return
        }

        let outputViewController = OutputViewController.initialiaze(
            withNumberOfEngineers: count,
            chanceToBecomeSuperBusy: chance)
        navigationController?.pushViewController(
            outputViewController,
            animated: true)
    }

    // MARK: - Errors
    private func showError(message: String) {
        let alert = UIAlertController(
            title: "Error",
            message: message,
            preferredStyle: .alert)
        alert.addAction(
            UIAlertAction(
                title: "Ok",
                style: .default,
                handler: nil))
        present(
            alert,
            animated: true)
    }
}
