// Created by Paulius Cesekas on 05/11/2018.

import UIKit
import Domain

class OutputViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet private weak var outputView: UITextView!
    @IBOutlet private weak var simulateButton: UIBarButtonItem!
    
    // MARK: - Variables
    private var simulator: Simulator!
    
    // MARK: - Methods -
    class func initialiaze(withNumberOfEngineers numberOfEngineers: Int,
                           chanceToBecomeSuperBusy: Double) -> OutputViewController {
        let storyboard = UIStoryboard(
            name: "Main",
            bundle: nil)
        guard let viewController = storyboard.instantiateViewController(withIdentifier: "OutputViewController")
            as? OutputViewController else {
                fatalError("`OutputViewController` is not configured properly in storyboard")
        }
        
        viewController.simulator = Simulator(
            numberOfEngineers: numberOfEngineers,
            chanceToBecomeSuperBusy: chanceToBecomeSuperBusy,
            speed: 100000)
        return viewController
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        simulator.delegate = self
        outputView.text = ""
        outputView.layoutManager.allowsNonContiguousLayout = false
    }
    
    // MARK: - UI Actions
    @IBAction
    private func didTouchUpInsideSimulateButton() {
        outputView.text = "" +
            "-----------------------\n" +
            "-  SIMULATION  START  -\n" +
            "-----------------------\n" +
            "\n"
        simulateButton.isEnabled = false
        simulator.reset()
        simulator.startSimulation()
    }
    
    // MARK: - Helpers
    fileprivate func scrollToBottom() {
        let lastLine = NSMakeRange(outputView.text.count - 1, 1)
        outputView.scrollRangeToVisible(lastLine)
    }
}

// MARK: - SimulatorDelegate
extension OutputViewController: SimulatorDelegate {
    func simulator(_ simulator: Simulator, didUpdate time: Time) {
        title = "T - \(time.formattedTwentyFourHours)"
    }
    
    func simulator(_ simulator: Simulator, didBrewItem item: Engineer) {
        outputView.text += "\nBrewed: \(item)"
        scrollToBottom()
    }
    
    func simulatorDidComplete(_ simulator: Simulator) {
        outputView.text += "\n\n" +
            "-----------------------\n" +
            "- SIMULATION COMPLETE -\n" +
            "-----------------------\n"
        scrollToBottom()
        simulateButton.isEnabled = true
    }
}
