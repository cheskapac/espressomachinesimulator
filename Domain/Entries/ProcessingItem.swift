// Created by Paulius Cesekas on 05/11/2018.

import Foundation

public class ProcessingItem {
    public var engineer: Engineer
    public var processTime: Time
    
    public init(_ engineer: Engineer) {
        self.engineer = engineer
        processTime = 0
    }
}
