// Created by Paulius Cesekas on 30/10/2018.

import Foundation

/// Time in seconds
public typealias Time = Double

// MARK: - Constants
extension Time {
    public static var second: Time = 1
    public static var minuteInSeconds: Time = 60
    public static var hourInMinutes: Time = 60
    public static var hourInSeconds: Time = 3600
}

// MARK: - Data access
extension Time {
    public var seconds: Double {
        return self
    }
    public var secondsAfterMinutes: Double {
        return self.truncatingRemainder(dividingBy: Time.minuteInSeconds)
    }
    public var minutes: Double {
        return floor(self / Time.minuteInSeconds)
    }
    public var minutesAfterHours: Double {
        return (self / Time.minuteInSeconds).truncatingRemainder(dividingBy: Time.hourInMinutes)
    }
    public var hours: Double {
        return floor(self / Time.hourInSeconds)
    }
}
