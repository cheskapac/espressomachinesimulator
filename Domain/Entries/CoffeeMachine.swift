// Created by Paulius Cesekas on 30/10/2018.

import Foundation

public struct CoffeeMachine {
    public var queue: [Engineer]
    public let queueProcessing: ProcessingType
    public let queuePriority: PriorityType
    public var itemInProcess: ProcessingItem?
    
    public init(queueProcessing: ProcessingType = .firstComeFirstServe,
                queuePriority: PriorityType = .superBusyFirst) {
        queue = [Engineer]()
        self.queueProcessing = queueProcessing
        self.queuePriority = queuePriority
    }
}
