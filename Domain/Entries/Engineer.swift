// Created by Paulius Cesekas on 30/10/2018.

import Foundation

open class Engineer {
    // MARK: - Variables
    public let id: String
    public var isSuperBusy: Bool
    public var coffeeConsumtionPerHour: Double
    public var coffee: CoffeeType
    public var workingHoursStart: Time

    // MARK: - Methods -
    public init(id: String,
                isSuperBusy: Bool = false,
                coffeeConsumtionPerHour: Double = 1,
                coffee: CoffeeType = .espresso,
                workingHoursStart: Time = 8 * Time.hourInSeconds) {
        self.id = id
        self.isSuperBusy = isSuperBusy
        self.coffeeConsumtionPerHour = coffeeConsumtionPerHour
        self.coffee = coffee
        self.workingHoursStart = workingHoursStart
    }
}
